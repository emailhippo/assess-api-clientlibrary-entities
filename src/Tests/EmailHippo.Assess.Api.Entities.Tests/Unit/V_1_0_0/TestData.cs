﻿// <copyright file="TestData.cs" company="Email Hippo Ltd">
// (c) 2020, Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.Tests.Unit.V_1_0_0
{
    using System.Collections.Generic;
    using EmailHippo.Assess.Api.Entities.V_1_0_0;
    using EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis;
    using EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis;
    using EmailHippo.Assess.Api.Entities.V_1_0_0.HippoTrust;
    using EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis;
    using EmailHippo.Assess.Api.Entities.V_1_0_0.Meta;
    using EmailHippo.Assess.Api.Entities.V_1_0_0.UserAnalysis;
    using EmailHippo.Assess.Api.Entities.V_1_0_0.Version;

    /// <summary>
    /// Test data.
    /// </summary>
    public static class TestData
    {
        /// <summary>
        /// Gets full Result.
        /// </summary>
        /// <value>
        /// The full result.
        /// </value>
        public static Result FullResult
        {
            get
            {
                var result = new Result
                {
                    Version = new Version
                    {
                        Doc = @"https://api-docs.emailhippo.com/en/latest",
                        V = @"ASSESS-(1.0.0)",
                    },
                    Meta = new Meta()
                    {
                        LastModified = "Thu, 28 May 2020 02:22:22 GMT",
                        Expires = "Thu, 28 July 2020 02:22 GMT",
                        Email = "me@test.com",
                        EmailHashMd5 = "a14d6a631f8f34f0118a716a0930a57d",
                        EmailHashSha1 = "130a23c858555ce9e86e73167383b20f6a614ba6",
                        EmailHashSha256 = "85483728e5c3fff8e0dbd6845455596b9568d08546be163b019cfc8253f804f7",
                        FirstName = "Fred",
                        LastName = "Smith",
                        Domain = "test",
                        Ip = "136.432.322.23",
                        Tld = "com",
                    },
                    HippoTrust = new HippoTrust()
                    {
                        Score = 0.1M,
                    },
                    UserAnalysis = new UserAnalysis()
                    {
                        UserDisposition = new UserDisposition()
                        {
                            IsGibberish = false,
                            IsProfanity = false,
                            Gender = GenderType.Male,
                        },
                    },
                    EmailAddressAnalysis = new EmailAddressAnalysis()
                    {
                        EmailAddressDisposition = new EmailAddressDisposition()
                        {
                            IsGibberish = false,
                            IsProfanity = false,
                            IsFreeMail = true,
                            IsRole = false,
                            IsSubaddressing = false,
                            HasBeenBlocked = false,
                            HasCatchAllServer = true,
                        },
                        MailboxVerification = new MailboxVerification()
                        {
                            Result = ResultType.Ok,
                            SecondaryResult = SecondaryResultType.Success,
                        },
                        EmailAddressSyntaxVerification = new EmailAddressSyntaxVerification()
                        {
                            IsSyntaxValid = true,
                        },
                        Assessments = new Assessments { ConsumerBusiness = 7 },
                    },
                    DomainAnalysis = new DomainAnalysis()
                    {
                        DomainDisposition = new DomainDisposition()
                        {
                            HasBeenBlocked = false,
                            IsGibberish = true,
                            HasDnsRecords = true,
                            HasMxRecords = false,
                            IsDarkWeb = true,
                            IsProfanity = false,
                            IsSubDomain = true,
                            MailServerInfrastructure = MailServerInfrastructureType.GoogleForBiz,
                            MailServerLocation = "USA",
                        },
                        Age = new Age()
                        {
                            Text = "22 year(s), 11 month(s), 4 week(s), 0 day(s)",
                            Seconds = 54747373737,
                            Iso8601 = "P22Y11M28D",
                        },
                        Registration = new Registration()
                        {
                            OwnerContactCity = "Jacksonville",
                            OwnerContactCountry = "USA",
                            OwnerContactName = "PERFECT PRIVACY, LLC",
                        },
                    },
                    IpAddressAnalysis = new IpAddressAnalysis()
                    {
                        IpDisposition = new IpDisposition()
                        {
                            HasBeenBlocked = false,
                            IsTorExitNode = true,
                        },
                        Location = new Location()
                        {
                            City = new City()
                            {
                                Name = "London",
                            },
                            Continent = new Continent()
                            {
                                Code = "EU",
                                Name = "Europe",
                            },
                            Country = new Country()
                            {
                                Name = "United Kingdom",
                                Code = "GB",
                                Currencies = new List<string>() { "GDP" },
                                DialingCodes = new List<string>() { "+44" },
                                IsInEU = false,
                            },
                            Latitude = 51.5074,
                            Longitude = -0.12775799999999998,
                            TimeZone = "Europe/London",
                        },
                        DataCenter = new DataCenter()
                        {
                            AutonomousSystemNumber = 2529,
                            AutonomousSystemOrganization = "Cable & Wireless Worldwide",
                            ConnectionType = "Corporate",
                            Isp = "DEMON",
                            Organization = "Vodafone Ltd",
                        },
                    },
                };

                return result;
            }
        }

        /// <summary>
        /// Gets the no ip result.
        /// </summary>
        /// <value>
        /// The no ip result.
        /// </value>
        public static Result NoIpResult
        {
            get
            {
                var result = FullResult;

                result.IpAddressAnalysis = null;

                return result;
            }
        }
    }
}