﻿// <copyright file="SerializeTests.cs" company="Email Hippo Ltd">
// (c) 2020, Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.Tests.Unit.V_1_0_0
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.Serialization;
    using EmailHippo.Assess.Api.Entities.V_1_0_0;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using ProtoBuf;

    /// <summary>
    /// The serializer tests.
    /// </summary>
    /// <seealso cref="TestBase" />
    [TestFixture]
    public class SerializeTests : TestBase
    {
        /// <summary>
        /// Serializes to json when full result expect serialize.
        /// </summary>
        [Test]
        public void SerializeToJson_WhenFullResult_ExpectSerialize()
        {
            // arrange
            var fullResult = TestData.FullResult;

            // act
            var stopwatch = Stopwatch.StartNew();
            var serializeObject = JsonConvert.SerializeObject(fullResult);
            stopwatch.Stop();

            // assert
            const string expectedResult = "{\"version\":{\"v\":\"ASSESS-(1.0.0)\",\"doc\":\"https://api-docs.emailhippo.com/en/latest\"},\"meta\":{\"lastModified\":\"Thu, 28 May 2020 02:22:22 GMT\",\"expires\":\"Thu, 28 July 2020 02:22 GMT\",\"email\":\"me@test.com\",\"emailHashMd5\":\"a14d6a631f8f34f0118a716a0930a57d\",\"emailHashSha1\":\"130a23c858555ce9e86e73167383b20f6a614ba6\",\"emailHashSha256\":\"85483728e5c3fff8e0dbd6845455596b9568d08546be163b019cfc8253f804f7\",\"firstName\":\"Fred\",\"lastName\":\"Smith\",\"domain\":\"test\",\"ip\":\"136.432.322.23\",\"tld\":\"com\"},\"hippoTrust\":{\"score\":0.1},\"userAnalysis\":{\"syntaxVerification\":null,\"disposition\":{\"isGibberish\":false,\"isProfanity\":false,\"gender\":\"Male\"}},\"emailAddressAnalysis\":{\"mailboxVerification\":{\"result\":\"Ok\",\"secondaryResult\":\"Success\"},\"syntaxVerification\":{\"isSyntaxValid\":true},\"disposition\":{\"isGibberish\":false,\"isProfanity\":false,\"isRole\":false,\"isSubaddressing\":false,\"isFreeMail\":true,\"hasCatchAllServer\":true,\"hasBeenBlocked\":false},\"assessments\":{\"consumerBusiness\":7}},\"domainAnalysis\":{\"disposition\":{\"isSubDomain\":true,\"isGibberish\":true,\"isProfanity\":false,\"isDarkWeb\":true,\"hasBeenBlocked\":false,\"hasDnsRecords\":true,\"hasMxRecords\":false,\"mailServerLocation\":\"USA\",\"mailServerInfrastructure\":\"GoogleForBiz\"},\"age\":{\"text\":\"22 year(s), 11 month(s), 4 week(s), 0 day(s)\",\"seconds\":54747373737,\"iso8601\":\"P22Y11M28D\"},\"registration\":{\"ownerContactName\":\"PERFECT PRIVACY, LLC\",\"ownerContactCity\":\"Jacksonville\",\"ownerContactCountry\":\"USA\"}},\"ipAddressAnalysis\":{\"disposition\":{\"isTorExitNode\":true,\"hasBeenBlocked\":false},\"location\":{\"latitude\":51.5074,\"longitude\":-0.12775799999999998,\"timeZone\":\"Europe/London\",\"continent\":{\"code\":\"EU\",\"name\":\"Europe\"},\"country\":{\"code\":\"GB\",\"name\":\"United Kingdom\",\"isInEU\":false,\"currencies\":[\"GDP\"],\"dialingCodes\":[\"+44\"]},\"city\":{\"name\":\"London\"}},\"dataCenter\":{\"autonomousSystemNumber\":2529,\"autonomousSystemOrganization\":\"Cable & Wireless Worldwide\",\"connectionType\":\"Corporate\",\"isp\":\"DEMON\",\"organization\":\"Vodafone Ltd\"}}}";

            Console.WriteLine(serializeObject);
            Console.WriteLine("Length:{0}", serializeObject.Length);
            Assert.That(expectedResult, Is.EqualTo(serializeObject));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Serializes to XML when full result expect serialize.
        /// </summary>
        [Test]
        public void SerializeToXml_WhenFullResult_ExpectSerialize()
        {
            // arrange
            var fullResult = TestData.FullResult;

            var dataContractSerializer = new DataContractSerializer(typeof(Result));

            string s;

            // act
            var stopwatch = Stopwatch.StartNew();
            using (var ms = new MemoryStream())
            {
                dataContractSerializer.WriteObject(ms, fullResult);

                ms.Position = 0;

                s = System.Text.Encoding.UTF8.GetString(ms.ToArray());
            }

            stopwatch.Stop();

            // assert
            const string expectedResult = @"<Result xmlns=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0"" xmlns:i=""http://www.w3.org/2001/XMLSchema-instance""><Version xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.Version""><a:V>ASSESS-(1.0.0)</a:V><a:Doc>https://api-docs.emailhippo.com/en/latest</a:Doc></Version><Meta xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.Meta""><a:LastModified>Thu, 28 May 2020 02:22:22 GMT</a:LastModified><a:Expires>Thu, 28 July 2020 02:22 GMT</a:Expires><a:Email>me@test.com</a:Email><a:EmailHashMd5>a14d6a631f8f34f0118a716a0930a57d</a:EmailHashMd5><a:EmailHashSha1>130a23c858555ce9e86e73167383b20f6a614ba6</a:EmailHashSha1><a:EmailHashSha256>85483728e5c3fff8e0dbd6845455596b9568d08546be163b019cfc8253f804f7</a:EmailHashSha256><a:FirstName>Fred</a:FirstName><a:LastName>Smith</a:LastName><a:Domain>test</a:Domain><a:Ip>136.432.322.23</a:Ip><a:Tld>com</a:Tld></Meta><HippoTrust xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.HippoTrust""><a:Score>0.1</a:Score></HippoTrust><UserAnalysis xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.UserAnalysis""><a:UserSyntaxVerification i:nil=""true""/><a:UserDisposition><a:IsGibberish>false</a:IsGibberish><a:IsProfanity>false</a:IsProfanity><a:Gender>Male</a:Gender></a:UserDisposition></UserAnalysis><EmailAddressAnalysis xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis""><a:MailboxVerification><a:Result>Ok</a:Result><a:SecondaryResult>Success</a:SecondaryResult></a:MailboxVerification><a:EmailAddressSyntaxVerification><a:IsSyntaxValid>true</a:IsSyntaxValid></a:EmailAddressSyntaxVerification><a:EmailAddressDisposition><a:IsGibberish>false</a:IsGibberish><a:IsProfanity>false</a:IsProfanity><a:IsRole>false</a:IsRole><a:IsSubaddressing>false</a:IsSubaddressing><a:IsFreeMail>true</a:IsFreeMail><a:HasCatchAllServer>true</a:HasCatchAllServer><a:HasBeenBlocked>false</a:HasBeenBlocked></a:EmailAddressDisposition><a:Assessments><a:ConsumerBusiness>7</a:ConsumerBusiness></a:Assessments></EmailAddressAnalysis><DomainAnalysis xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis""><a:DomainDisposition><a:IsSubDomain>true</a:IsSubDomain><a:IsGibberish>true</a:IsGibberish><a:IsProfanity>false</a:IsProfanity><a:IsDarkWeb>true</a:IsDarkWeb><a:HasBeenBlocked>false</a:HasBeenBlocked><a:HasDnsRecords>true</a:HasDnsRecords><a:HasMxRecords>false</a:HasMxRecords><a:MailServerLocation>USA</a:MailServerLocation><a:MailServerInfrastructure>GoogleForBiz</a:MailServerInfrastructure></a:DomainDisposition><a:Age><a:Text>22 year(s), 11 month(s), 4 week(s), 0 day(s)</a:Text><a:Seconds>54747373737</a:Seconds><a:Iso8601>P22Y11M28D</a:Iso8601></a:Age><a:Registration><a:OwnerContactName>PERFECT PRIVACY, LLC</a:OwnerContactName><a:OwnerContactCity>Jacksonville</a:OwnerContactCity><a:OwnerContactCountry>USA</a:OwnerContactCountry></a:Registration></DomainAnalysis><IpAddressAnalysis xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis""><a:IpDisposition><a:IsTorExitNode>true</a:IsTorExitNode><a:HasBeenBlocked>false</a:HasBeenBlocked></a:IpDisposition><a:Location><a:Latitude>51.5074</a:Latitude><a:Longitude>-0.12775799999999998</a:Longitude><a:TimeZone>Europe/London</a:TimeZone><a:Continent><a:Code>EU</a:Code><a:Name>Europe</a:Name></a:Continent><a:Country><a:Code>GB</a:Code><a:Name>United Kingdom</a:Name><a:IsInEU>false</a:IsInEU><a:Currencies xmlns:b=""http://schemas.microsoft.com/2003/10/Serialization/Arrays""><b:string>GDP</b:string></a:Currencies><a:DialingCodes xmlns:b=""http://schemas.microsoft.com/2003/10/Serialization/Arrays""><b:string>+44</b:string></a:DialingCodes></a:Country><a:City><a:Name>London</a:Name></a:City></a:Location><a:DataCenter><a:AutonomousSystemNumber>2529</a:AutonomousSystemNumber><a:AutonomousSystemOrganization>Cable &amp; Wireless Worldwide</a:AutonomousSystemOrganization><a:ConnectionType>Corporate</a:ConnectionType><a:Isp>DEMON</a:Isp><a:Organization>Vodafone Ltd</a:Organization></a:DataCenter></IpAddressAnalysis></Result>";

            Console.WriteLine(s);
            Console.WriteLine("Length:{0}", s.Length);
            Assert.That(expectedResult, Is.EqualTo(s));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Serializes to protobuf when full result expect serialize and deserialize.
        /// </summary>
        [Test]
        public void SerializeToProtobuf_WhenFullResult_ExpectSerializeAndDeserialize()
        {
            // arrange
            var fullResult = TestData.FullResult;

            string s;

            // act
            var stopwatch = Stopwatch.StartNew();
            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, fullResult);
                ms.Position = 0;

                ms.TryGetBuffer(out ArraySegment<byte> buffer);

                s = Convert.ToBase64String(buffer.Array, 0, (int)ms.Length);
            }

            Result deserialized;

            var bytes = Convert.FromBase64String(s);

            using (var ms = new MemoryStream(bytes))
            {
                ms.Position = 0;

                deserialized = Serializer.Deserialize<Result>(ms);
            }

            stopwatch.Stop();

            // assert
            const string expectedResult = @"CjsKDkFTU0VTUy0oMS4wLjApEilodHRwczovL2FwaS1kb2NzLmVtYWlsaGlwcG8uY29tL2VuL2xhdGVzdBL/AQodVGh1LCAyOCBNYXkgMjAyMCAwMjoyMjoyMiBHTVQSG1RodSwgMjggSnVseSAyMDIwIDAyOjIyIEdNVBoLbWVAdGVzdC5jb20iIGExNGQ2YTYzMWY4ZjM0ZjAxMThhNzE2YTA5MzBhNTdkKigxMzBhMjNjODU4NTU1Y2U5ZTg2ZTczMTY3MzgzYjIwZjZhNjE0YmE2MkA4NTQ4MzcyOGU1YzNmZmY4ZTBkYmQ2ODQ1NDU1NTk2Yjk1NjhkMDg1NDZiZTE2M2IwMTljZmM4MjUzZjgwNGY3OgRGcmVkQgVTbWl0aEoEdGVzdFIOMTM2LjQzMi4zMjIuMjNaA2NvbRoGCgQIARgCIgQSAhgBKhQKBAgBEAESAggBGgQoATABIgIIBzJ/Cg8IARABIAEwAUIDVVNBSBMSQQosMjIgeWVhcihzKSwgMTEgbW9udGgocyksIDQgd2VlayhzKSwgMCBkYXkocykQqcHK+csBGgpQMjJZMTFNMjhEGikKFFBFUkZFQ1QgUFJJVkFDWSwgTExDEgxKYWNrc29udmlsbGUaA1VTQTqgAQoCCAESWQnF/rJ78sBJQBH+s+bHX1rAvxoNRXVyb3BlL0xvbmRvbiIMCgJFVRIGRXVyb3BlKh4KAkdCEg5Vbml0ZWQgS2luZ2RvbSIDR0RQKgMrNDQyCAoGTG9uZG9uGj8I4RMSGkNhYmxlICYgV2lyZWxlc3MgV29ybGR3aWRlGglDb3Jwb3JhdGUiBURFTU9OKgxWb2RhZm9uZSBMdGQ=";

            Console.WriteLine(s);
            Console.WriteLine("Length:{0}", s.Length);
            Assert.That(expectedResult, Is.EqualTo(s));
            Assert.That(deserialized.Meta.Domain == @"test");
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Serializes to json when no ip result expect serialize.
        /// </summary>
        [Test]
        public void SerializeToJson_WhenNoIpResult_ExpectSerialize()
        {
            // arrange
            var fullResult = TestData.NoIpResult;

            // act
            var stopwatch = Stopwatch.StartNew();
            var serializeObject = JsonConvert.SerializeObject(fullResult);
            stopwatch.Stop();

            // assert
            const string expectedResult = @"{""version"":{""v"":""ASSESS-(1.0.0)"",""doc"":""https://api-docs.emailhippo.com/en/latest""},""meta"":{""lastModified"":""Thu, 28 May 2020 02:22:22 GMT"",""expires"":""Thu, 28 July 2020 02:22 GMT"",""email"":""me@test.com"",""emailHashMd5"":""a14d6a631f8f34f0118a716a0930a57d"",""emailHashSha1"":""130a23c858555ce9e86e73167383b20f6a614ba6"",""emailHashSha256"":""85483728e5c3fff8e0dbd6845455596b9568d08546be163b019cfc8253f804f7"",""firstName"":""Fred"",""lastName"":""Smith"",""domain"":""test"",""ip"":""136.432.322.23"",""tld"":""com""},""hippoTrust"":{""score"":0.1},""userAnalysis"":{""syntaxVerification"":null,""disposition"":{""isGibberish"":false,""isProfanity"":false,""gender"":""Male""}},""emailAddressAnalysis"":{""mailboxVerification"":{""result"":""Ok"",""secondaryResult"":""Success""},""syntaxVerification"":{""isSyntaxValid"":true},""disposition"":{""isGibberish"":false,""isProfanity"":false,""isRole"":false,""isSubaddressing"":false,""isFreeMail"":true,""hasCatchAllServer"":true,""hasBeenBlocked"":false},""assessments"":{""consumerBusiness"":7}},""domainAnalysis"":{""disposition"":{""isSubDomain"":true,""isGibberish"":true,""isProfanity"":false,""isDarkWeb"":true,""hasBeenBlocked"":false,""hasDnsRecords"":true,""hasMxRecords"":false,""mailServerLocation"":""USA"",""mailServerInfrastructure"":""GoogleForBiz""},""age"":{""text"":""22 year(s), 11 month(s), 4 week(s), 0 day(s)"",""seconds"":54747373737,""iso8601"":""P22Y11M28D""},""registration"":{""ownerContactName"":""PERFECT PRIVACY, LLC"",""ownerContactCity"":""Jacksonville"",""ownerContactCountry"":""USA""}},""ipAddressAnalysis"":null}";

            Console.WriteLine(serializeObject);
            Console.WriteLine("Length:{0}", serializeObject.Length);
            Assert.That(expectedResult, Is.EqualTo(serializeObject));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Serializes to XML when no ip result expect serialize.
        /// </summary>
        [Test]
        public void SerializeToXml_WhenNoIpResult_ExpectSerialize()
        {
            // arrange
            var fullResult = TestData.NoIpResult;

            var dataContractSerializer = new DataContractSerializer(typeof(Result));

            string s;

            // act
            var stopwatch = Stopwatch.StartNew();
            using (var ms = new MemoryStream())
            {
                dataContractSerializer.WriteObject(ms, fullResult);

                ms.Position = 0;

                s = System.Text.Encoding.UTF8.GetString(ms.ToArray());
            }

            stopwatch.Stop();

            // assert
            const string expectedResult = @"<Result xmlns=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0"" xmlns:i=""http://www.w3.org/2001/XMLSchema-instance""><Version xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.Version""><a:V>ASSESS-(1.0.0)</a:V><a:Doc>https://api-docs.emailhippo.com/en/latest</a:Doc></Version><Meta xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.Meta""><a:LastModified>Thu, 28 May 2020 02:22:22 GMT</a:LastModified><a:Expires>Thu, 28 July 2020 02:22 GMT</a:Expires><a:Email>me@test.com</a:Email><a:EmailHashMd5>a14d6a631f8f34f0118a716a0930a57d</a:EmailHashMd5><a:EmailHashSha1>130a23c858555ce9e86e73167383b20f6a614ba6</a:EmailHashSha1><a:EmailHashSha256>85483728e5c3fff8e0dbd6845455596b9568d08546be163b019cfc8253f804f7</a:EmailHashSha256><a:FirstName>Fred</a:FirstName><a:LastName>Smith</a:LastName><a:Domain>test</a:Domain><a:Ip>136.432.322.23</a:Ip><a:Tld>com</a:Tld></Meta><HippoTrust xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.HippoTrust""><a:Score>0.1</a:Score></HippoTrust><UserAnalysis xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.UserAnalysis""><a:UserSyntaxVerification i:nil=""true""/><a:UserDisposition><a:IsGibberish>false</a:IsGibberish><a:IsProfanity>false</a:IsProfanity><a:Gender>Male</a:Gender></a:UserDisposition></UserAnalysis><EmailAddressAnalysis xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis""><a:MailboxVerification><a:Result>Ok</a:Result><a:SecondaryResult>Success</a:SecondaryResult></a:MailboxVerification><a:EmailAddressSyntaxVerification><a:IsSyntaxValid>true</a:IsSyntaxValid></a:EmailAddressSyntaxVerification><a:EmailAddressDisposition><a:IsGibberish>false</a:IsGibberish><a:IsProfanity>false</a:IsProfanity><a:IsRole>false</a:IsRole><a:IsSubaddressing>false</a:IsSubaddressing><a:IsFreeMail>true</a:IsFreeMail><a:HasCatchAllServer>true</a:HasCatchAllServer><a:HasBeenBlocked>false</a:HasBeenBlocked></a:EmailAddressDisposition><a:Assessments><a:ConsumerBusiness>7</a:ConsumerBusiness></a:Assessments></EmailAddressAnalysis><DomainAnalysis xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis""><a:DomainDisposition><a:IsSubDomain>true</a:IsSubDomain><a:IsGibberish>true</a:IsGibberish><a:IsProfanity>false</a:IsProfanity><a:IsDarkWeb>true</a:IsDarkWeb><a:HasBeenBlocked>false</a:HasBeenBlocked><a:HasDnsRecords>true</a:HasDnsRecords><a:HasMxRecords>false</a:HasMxRecords><a:MailServerLocation>USA</a:MailServerLocation><a:MailServerInfrastructure>GoogleForBiz</a:MailServerInfrastructure></a:DomainDisposition><a:Age><a:Text>22 year(s), 11 month(s), 4 week(s), 0 day(s)</a:Text><a:Seconds>54747373737</a:Seconds><a:Iso8601>P22Y11M28D</a:Iso8601></a:Age><a:Registration><a:OwnerContactName>PERFECT PRIVACY, LLC</a:OwnerContactName><a:OwnerContactCity>Jacksonville</a:OwnerContactCity><a:OwnerContactCountry>USA</a:OwnerContactCountry></a:Registration></DomainAnalysis><IpAddressAnalysis i:nil=""true"" xmlns:a=""http://schemas.datacontract.org/2004/07/EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis""/></Result>";

            Console.WriteLine(s);
            Console.WriteLine("Length:{0}", s.Length);
            Assert.That(expectedResult, Is.EqualTo(s));
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Serializes to protobuf when no ip result expect serialize and deserialize.
        /// </summary>
        [Test]
        public void SerializeToProtobuf_WhenNoIpResult_ExpectSerializeAndDeserialize()
        {
            // arrange
            var fullResult = TestData.NoIpResult;

            string s;

            // act
            var stopwatch = Stopwatch.StartNew();
            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, fullResult);
                ms.Position = 0;

                ms.TryGetBuffer(out ArraySegment<byte> buffer);

                s = Convert.ToBase64String(buffer.Array, 0, (int)ms.Length);
            }

            Result deserialized;

            var bytes = Convert.FromBase64String(s);

            using (var ms = new MemoryStream(bytes))
            {
                ms.Position = 0;

                deserialized = Serializer.Deserialize<Result>(ms);
            }

            stopwatch.Stop();

            // assert
            const string expectedResult = @"CjsKDkFTU0VTUy0oMS4wLjApEilodHRwczovL2FwaS1kb2NzLmVtYWlsaGlwcG8uY29tL2VuL2xhdGVzdBL/AQodVGh1LCAyOCBNYXkgMjAyMCAwMjoyMjoyMiBHTVQSG1RodSwgMjggSnVseSAyMDIwIDAyOjIyIEdNVBoLbWVAdGVzdC5jb20iIGExNGQ2YTYzMWY4ZjM0ZjAxMThhNzE2YTA5MzBhNTdkKigxMzBhMjNjODU4NTU1Y2U5ZTg2ZTczMTY3MzgzYjIwZjZhNjE0YmE2MkA4NTQ4MzcyOGU1YzNmZmY4ZTBkYmQ2ODQ1NDU1NTk2Yjk1NjhkMDg1NDZiZTE2M2IwMTljZmM4MjUzZjgwNGY3OgRGcmVkQgVTbWl0aEoEdGVzdFIOMTM2LjQzMi4zMjIuMjNaA2NvbRoGCgQIARgCIgQSAhgBKhQKBAgBEAESAggBGgQoATABIgIIBzJ/Cg8IARABIAEwAUIDVVNBSBMSQQosMjIgeWVhcihzKSwgMTEgbW9udGgocyksIDQgd2VlayhzKSwgMCBkYXkocykQqcHK+csBGgpQMjJZMTFNMjhEGikKFFBFUkZFQ1QgUFJJVkFDWSwgTExDEgxKYWNrc29udmlsbGUaA1VTQQ==";

            Console.WriteLine(s);
            Console.WriteLine("Length:{0}", s.Length);
            Assert.That(expectedResult, Is.EqualTo(s));
            Assert.That(deserialized.Meta.Domain == @"test");
            Assert.That(deserialized.IpAddressAnalysis, Is.Null);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }
    }
}
