﻿// <copyright file="UserDisposition.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.UserAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using ProtoBuf;

    /// <summary>
    /// User Analysis Disposition.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = "UserDisposition")]
    [Serializable]
    public sealed class UserDisposition
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is gibberish.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is gibberish; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"IsGibberish", IsRequired = false, Order = 1)]
        [JsonProperty(PropertyName = @"isGibberish", Order = 1)]
        public bool IsGibberish { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is profanity.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is profanity; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"IsProfanity", IsRequired = false, Order = 2)]
        [JsonProperty(PropertyName = @"isProfanity", Order = 2)]
        public bool IsProfanity { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        /// <value>
        /// The gender.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"Gender", IsRequired = false, Order = 3)]
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = @"gender", Order = 3)]
        public GenderType Gender { get; set; }
    }
}