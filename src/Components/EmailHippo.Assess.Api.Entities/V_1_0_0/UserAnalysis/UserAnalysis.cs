﻿// <copyright file="UserAnalysis.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.UserAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// User Analysis.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(UserAnalysis))]
    [Serializable]
    public sealed class UserAnalysis
    {
        /// <summary>
        /// Gets or sets the user syntax verification.
        /// </summary>
        /// <value>
        /// The user syntax verification.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"UserSyntaxVerification", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"syntaxVerification", Order = 1)]
        public UserSyntaxVerification UserSyntaxVerification { get; set; }

        /// <summary>
        /// Gets or sets the disposition.
        /// </summary>
        /// <value>
        /// The disposition.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"UserDisposition", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"disposition", Order = 2)]
        public UserDisposition UserDisposition { get; set; }
    }
}