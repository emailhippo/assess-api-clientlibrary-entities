﻿// <copyright file="Location.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Ip Analysis Location.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(Location))]
    [Serializable]
    public sealed class Location
    {
        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>
        /// The latitude.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"Latitude", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"latitude", Order = 1)]
        public double? Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>
        /// The longitude.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"Longitude", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"longitude", Order = 2)]
        public double? Longitude { get; set; }

        /// <summary>
        /// Gets or sets the time zone.
        /// </summary>
        /// <value>
        /// The time zone.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"TimeZone", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"timeZone", Order = 3)]
        public string TimeZone { get; set; }

        /// <summary>
        /// Gets or sets the continent.
        /// </summary>
        /// <value>
        /// The continent.
        /// </value>
        [ProtoMember(4)]
        [DataMember(Name = @"Continent", IsRequired = true, Order = 4)]
        [JsonProperty(PropertyName = @"continent", Order = 4)]
        public Continent Continent { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [ProtoMember(5)]
        [DataMember(Name = @"Country", IsRequired = true, Order = 5)]
        [JsonProperty(PropertyName = @"country", Order = 5)]
        public Country Country { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        [ProtoMember(6)]
        [DataMember(Name = @"City", IsRequired = true, Order = 6)]
        [JsonProperty(PropertyName = @"city", Order = 6)]
        public City City { get; set; }
    }
}