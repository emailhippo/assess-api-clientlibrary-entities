﻿// <copyright file="DataCenter.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Ip Analysis DataCenter.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(DataCenter))]
    [Serializable]
    public sealed class DataCenter
    {
        /// <summary>
        /// Gets or sets the autonomous system number.
        /// </summary>
        /// <value>
        /// The autonomous system number.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"AutonomousSystemNumber", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"autonomousSystemNumber", Order = 1)]
        public long? AutonomousSystemNumber { get; set; }

        /// <summary>
        /// Gets or sets the autonomous system organization.
        /// </summary>
        /// <value>
        /// The autonomous system organization.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"AutonomousSystemOrganization", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"autonomousSystemOrganization", Order = 2)]
        public string AutonomousSystemOrganization { get; set; }

        /// <summary>
        /// Gets or sets the type of the connection.
        /// </summary>
        /// <value>
        /// The type of the connection.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"ConnectionType", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"connectionType", Order = 3)]
        public string ConnectionType { get; set; }

        /// <summary>
        /// Gets or sets the isp.
        /// </summary>
        /// <value>
        /// The isp.
        /// </value>
        [ProtoMember(4)]
        [DataMember(Name = @"Isp", IsRequired = true, Order = 4)]
        [JsonProperty(PropertyName = @"isp", Order = 4)]
        public string Isp { get; set; }

        /// <summary>
        /// Gets or sets the organization.
        /// </summary>
        /// <value>
        /// The organization.
        /// </value>
        [ProtoMember(5)]
        [DataMember(Name = @"Organization", IsRequired = true, Order = 5)]
        [JsonProperty(PropertyName = @"organization", Order = 5)]
        public string Organization { get; set; }
    }
}