﻿// <copyright file="Country.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Ip Analysis Country.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(Country))]
    [Serializable]
    public sealed class Country
    {
        /// <summary>
        /// Gets or sets the iso code.
        /// </summary>
        /// <value>
        /// The iso code.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"Code", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"code", Order = 1)]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name en.
        /// </summary>
        /// <value>
        /// The name en.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"Name", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"name", Order = 2)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in eu.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is in eu; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"IsInEU", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"isInEU", Order = 3)]
        public bool IsInEU { get; set; }

        /// <summary>
        /// Gets or sets the currencies.
        /// </summary>
        /// <value>
        /// The currency.
        /// </value>
        [ProtoMember(4)]
        [DataMember(Name = @"Currencies", IsRequired = true, Order = 4)]
        [JsonProperty(PropertyName = @"currencies", Order = 4)]
        public List<string> Currencies { get; set; }

        /// <summary>
        /// Gets or sets the DialingCodes.
        /// </summary>
        /// <value>
        /// The DialingCodes.
        /// </value>
        [ProtoMember(5)]
        [DataMember(Name = @"DialingCodes", IsRequired = true, Order = 5)]
        [JsonProperty(PropertyName = @"dialingCodes", Order = 5)]
        public List<string> DialingCodes { get; set; }
    }
}