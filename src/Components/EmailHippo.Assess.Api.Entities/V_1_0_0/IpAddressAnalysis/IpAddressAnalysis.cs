﻿// <copyright file="IpAddressAnalysis.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Ip Analysis.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(IpAddressAnalysis))]
    [Serializable]
    public sealed class IpAddressAnalysis
    {
        /// <summary>
        /// Gets or sets the disposition.
        /// </summary>
        /// <value>
        /// The disposition.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"IpDisposition", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"disposition", Order = 1)]
        public IpDisposition IpDisposition { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"Location", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"location", Order = 2)]
        public Location Location { get; set; }

        /// <summary>
        /// Gets or sets the data center.
        /// </summary>
        /// <value>
        /// The data center.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"DataCenter", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"dataCenter", Order = 3)]
        public DataCenter DataCenter { get; set; }
    }
}