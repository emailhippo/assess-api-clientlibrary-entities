﻿// <copyright file="IpDisposition.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.IpAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Ip Analysis Disposition.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = "IpDisposition")]
    [Serializable]
    public sealed class IpDisposition
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is tor exit node.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is tor exit node; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"IsTorExitNode", IsRequired = false, Order = 1)]
        [JsonProperty(PropertyName = @"isTorExitNode", Order = 1)]
        public bool IsTorExitNode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has been blocked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has been blocked; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"HasBeenBlocked", IsRequired = false, Order = 2)]
        [JsonProperty(PropertyName = @"hasBeenBlocked", Order = 2)]
        public bool HasBeenBlocked { get; set; }
    }
}