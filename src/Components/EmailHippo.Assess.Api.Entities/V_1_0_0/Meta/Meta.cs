﻿// <copyright file="Meta.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.Meta
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// The Meta.
    /// </summary>
    [ProtoContract]
    [Serializable]
    [DataContract(Name = nameof(Meta))]
    public sealed class Meta
    {
        /// <summary>
        /// Gets or sets the last modified.
        /// </summary>
        /// <value>
        /// The last modified.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"LastModified", IsRequired = false, Order = 1)]
        [JsonProperty(PropertyName = "lastModified", Order = 1)]
        public string LastModified { get; set; }

        /// <summary>
        /// Gets or sets the expires.
        /// </summary>
        /// <value>
        /// The expires.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"Expires", IsRequired = false, Order = 2)]
        [JsonProperty(PropertyName = "expires", Order = 2)]
        public string Expires { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"Email", IsRequired = false, Order = 3)]
        [JsonProperty(PropertyName = "email", Order = 3)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the email address hash MD5.
        /// </summary>
        /// <value>
        /// The email address hash MD5.
        /// </value>
        [ProtoMember(4)]
        [DataMember(Name = @"EmailHashMd5", IsRequired = false, Order = 4)]
        [JsonProperty(PropertyName = "emailHashMd5", Order = 4)]
        public string EmailHashMd5 { get; set; }

        /// <summary>
        /// Gets or sets the email address hash sha1.
        /// </summary>
        /// <value>
        /// The email address hash sha1.
        /// </value>
        [ProtoMember(5)]
        [DataMember(Name = @"EmailHashSha1", IsRequired = false, Order = 5)]
        [JsonProperty(PropertyName = "emailHashSha1", Order = 5)]
        public string EmailHashSha1 { get; set; }

        /// <summary>
        /// Gets or sets the email address hash sha265.
        /// </summary>
        /// <value>
        /// The email address hash sha265.
        /// </value>
        [ProtoMember(6)]
        [DataMember(Name = @"EmailHashSha256", IsRequired = false, Order = 6)]
        [JsonProperty(PropertyName = "emailHashSha256", Order = 6)]
        public string EmailHashSha256 { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [ProtoMember(7)]
        [DataMember(Name = @"FirstName", IsRequired = false, Order = 7)]
        [JsonProperty(PropertyName = "firstName", Order = 7)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [ProtoMember(8)]
        [DataMember(Name = @"LastName", IsRequired = false, Order = 8)]
        [JsonProperty(PropertyName = "lastName", Order = 8)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        [ProtoMember(9)]
        [DataMember(Name = @"Domain", IsRequired = false, Order = 9)]
        [JsonProperty(PropertyName = "domain", Order = 9)]
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets the ip.
        /// </summary>
        /// <value>
        /// The ip.
        /// </value>
        [ProtoMember(10)]
        [DataMember(Name = @"Ip", IsRequired = false, Order = 10)]
        [JsonProperty(PropertyName = "ip", Order = 10)]
        public string Ip { get; set; }

        /// <summary>
        /// Gets or sets the TLD.
        /// </summary>
        /// <value>
        /// The TLD.
        /// </value>
        [ProtoMember(11)]
        [DataMember(Name = @"Tld", IsRequired = false, Order = 11)]
        [JsonProperty(PropertyName = "tld", Order = 11)]
        public string Tld { get; set; }
    }
}