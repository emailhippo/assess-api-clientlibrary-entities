﻿// <copyright file="Result.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0
{
    using System;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// The result.
    /// </summary>
    [ProtoContract]
    [Serializable]
    [DataContract(Name = nameof(Result))]
    [XmlType(nameof(Result), AnonymousType = true, IncludeInSchema = false)]
    [XmlRoot(nameof(Result))]
    public sealed class Result
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>
        /// The version.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"Version", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"version", Order = 1)]
        public Version.Version Version { get; set; }

        /// <summary>
        /// Gets or sets the meta.
        /// </summary>
        /// <value>
        /// The meta.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name= @"Meta", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"meta", Order = 2)]
        public Meta.Meta Meta { get; set; }

        /// <summary>
        /// Gets or sets the hippo trust.
        /// </summary>
        /// <value>
        /// The hippo trust.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"HippoTrust", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"hippoTrust", Order = 3)]
        public HippoTrust.HippoTrust HippoTrust { get; set; }

        /// <summary>
        /// Gets or sets the user analysis.
        /// </summary>
        /// <value>
        /// The user analysis.
        /// </value>
        [ProtoMember(4)]
        [DataMember(Name = @"UserAnalysis", IsRequired = true, Order = 4)]
        [JsonProperty(PropertyName = @"userAnalysis", Order = 4)]
        public UserAnalysis.UserAnalysis UserAnalysis { get; set; }

        /// <summary>
        /// Gets or sets the email address analysis.
        /// </summary>
        /// <value>
        /// The email address analysis.
        /// </value>
        [ProtoMember(5)]
        [DataMember(Name = @"EmailAddressAnalysis", IsRequired = true, Order = 5)]
        [JsonProperty(PropertyName = @"emailAddressAnalysis", Order = 5)]
        public EmailAddressAnalysis.EmailAddressAnalysis EmailAddressAnalysis { get; set; }

        /// <summary>
        /// Gets or sets the domain analysis.
        /// </summary>
        /// <value>
        /// The domain analysis.
        /// </value>
        [ProtoMember(6)]
        [DataMember(Name = @"DomainAnalysis", IsRequired = true, Order = 6)]
        [JsonProperty(PropertyName = @"domainAnalysis", Order = 6)]
        public DomainAnalysis.DomainAnalysis DomainAnalysis { get; set; }

        /// <summary>
        /// Gets or sets the ip analysis.
        /// </summary>
        /// <value>
        /// The ip analysis.
        /// </value>
        [ProtoMember(7)]
        [DataMember(Name = @"IpAddressAnalysis", IsRequired = false, Order = 7)]
        [JsonProperty(PropertyName = @"ipAddressAnalysis", Order = 7)]
        public IpAddressAnalysis.IpAddressAnalysis IpAddressAnalysis { get; set; }
    }
}