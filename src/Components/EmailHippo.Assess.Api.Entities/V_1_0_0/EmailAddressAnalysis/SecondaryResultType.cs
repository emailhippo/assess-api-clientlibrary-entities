﻿// <copyright file="SecondaryResultType.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis
{
    /// <summary>
    /// The Email Address Analysis Secondary Result Type.
    /// </summary>
    public enum SecondaryResultType
    {
        /// <summary>
        /// <para>No additional information is available.</para>
        /// </summary>
        None,

        /// <summary>
        /// <para>Successful verification.</para>
        /// </summary>
        Success,

        /// <summary>
        /// <para>The domain (i.e. the bit after the '@' character) defined in the email address does not exist, according to DNS records.</para>
        /// </summary>
        DomainDoesNotExist,

        /// <summary>
        /// <para>The mailbox is full.</para>
        /// </summary>
        MailboxFull,

        /// <summary>
        /// <para>The mailbox does not exist.</para>
        /// </summary>
        MailboxDoesNotExist,

        /// <summary>
        /// <para>
        /// Unspecified mail server fault detected.
        /// </para>
        /// </summary>
        MailServerFaultDetected,

        /// <summary>
        /// <para>There are no mail servers defined for this domain, according to DNS.</para>
        /// </summary>
        NoMailServersForDomain,

        /// <summary>
        /// <para>Email address contains international characters.</para>
        /// </summary>
        EmailContainsInternationalCharacters,

        /// <summary>
        /// <para>Invalid email address format.</para>
        /// </summary>
        InvalidEmailFormat,

        /// <summary>
        /// <para>The email address is found on one or more block lists.</para>
        /// </summary>
        EmailAddressFoundOnBlockLists,

        /// <summary>
        /// <para>The remote email server timed out.</para>
        /// </summary>
        Timeout,

        /// <summary>
        /// <para>Grey listing is in operation. It is not possible to validate email boxes in real-time where grey listing is in operation.</para>
        /// </summary>
        MailServerGreyListing,

        /// <summary>
        /// <para>The server is configured for catch all and responds to all email verifications with a status of Ok.</para>
        /// </summary>
        MailServerCatchAll,

        /// <summary>
        /// <para>The email server is using anti-spam measures.</para>
        /// </summary>
        MailServerAntiSpam,

        /// <summary>
        /// <para>Unpredictable system infrastructure detected.</para>
        /// </summary>
        UnpredictableSystem,

        /// <summary>
        /// <para>A possible spam trap email address or domain has been detected.</para>
        /// </summary>
        PossibleSpamTrapDetected,

        /// <summary>
        /// <para>The domain is a well known Disposable Email Address (DEA).</para>
        /// </summary>
        DisposableEmailAddress,
    }
}