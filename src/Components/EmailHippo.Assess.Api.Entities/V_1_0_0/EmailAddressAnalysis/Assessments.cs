// <copyright file="Assessments.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

namespace EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// The <see cref="Assessments"/>.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = "Assessments")]
    [Serializable]
    public sealed class Assessments
    {
        /// <summary>
        /// Gets or sets the consumer / business relative scoring.
        /// </summary>
        [ProtoMember(1)]
        [DataMember(Name = @"ConsumerBusiness", IsRequired = true, Order = 1)]
        [JsonProperty("consumerBusiness")]
        public int ConsumerBusiness { get; set; }
    }
}