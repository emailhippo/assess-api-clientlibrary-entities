﻿// <copyright file="EmailAddressDisposition.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Email Address Analysis Disposition.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = "EmailAddressDisposition")]
    [Serializable]
    public sealed class EmailAddressDisposition
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is gibberish.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is gibberish; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"IsGibberish", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"isGibberish", Order = 1)]
        public bool IsGibberish { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is profanity.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is profanity; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"IsProfanity", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"isProfanity", Order = 2)]
        public bool IsProfanity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is role.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is role; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"IsRole", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"isRole", Order = 3)]
        public bool IsRole { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is subaddressing.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is subaddressing; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(4)]
        [DataMember(Name = @"IsSubaddressing", IsRequired = true, Order = 4)]
        [JsonProperty(PropertyName = @"isSubaddressing", Order = 4)]
        public bool IsSubaddressing { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is free mail.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is free mail; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(5)]
        [DataMember(Name = @"IsFreeMail", IsRequired = true, Order = 5)]
        [JsonProperty(PropertyName = @"isFreeMail", Order = 5)]
        public bool IsFreeMail { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has catch all server.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has catch all server; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(6)]
        [DataMember(Name = @"HasCatchAllServer", IsRequired = true, Order = 6)]
        [JsonProperty(PropertyName = @"hasCatchAllServer", Order = 6)]
        public bool HasCatchAllServer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has been blocked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has been blocked; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(7)]
        [DataMember(Name = @"HasBeenBlocked", IsRequired = true, Order = 7)]
        [JsonProperty(PropertyName = @"hasBeenBlocked", Order = 7)]
        public bool HasBeenBlocked { get; set; }
    }
}