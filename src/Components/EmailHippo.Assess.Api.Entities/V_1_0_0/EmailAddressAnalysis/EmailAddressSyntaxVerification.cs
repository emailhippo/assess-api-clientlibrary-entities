﻿// <copyright file="EmailAddressSyntaxVerification.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Email Address Analysis Syntax verification.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(EmailAddressSyntaxVerification))]
    [Serializable]
    public sealed class EmailAddressSyntaxVerification
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is syntax valid.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is syntax valid; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"IsSyntaxValid", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"isSyntaxValid", Order = 1)]
        public bool IsSyntaxValid { get; set; }
    }
}