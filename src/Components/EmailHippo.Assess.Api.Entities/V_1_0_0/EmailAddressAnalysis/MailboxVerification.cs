﻿// <copyright file="MailboxVerification.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using ProtoBuf;

    /// <summary>
    /// Email Address Analysis Mailbox verification.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(MailboxVerification))]
    [Serializable]
    public sealed class MailboxVerification
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"Result", IsRequired = true, Order = 1)]
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = @"result", Order = 1)]
        public ResultType Result { get; set; }

        /// <summary>
        /// Gets or sets the secondary result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"SecondaryResult", IsRequired = true, Order = 1)]
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = @"secondaryResult", Order = 1)]
        public SecondaryResultType SecondaryResult { get; set; }
    }
}