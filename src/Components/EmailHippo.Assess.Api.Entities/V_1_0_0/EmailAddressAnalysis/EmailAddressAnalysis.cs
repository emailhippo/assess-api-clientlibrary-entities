﻿// <copyright file="EmailAddressAnalysis.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Email Address Analysis.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = "EmailAddressAnalysis")]
    [Serializable]
    public sealed class EmailAddressAnalysis
    {
        /// <summary>
        /// Gets or sets the <see cref="MailboxVerification"/>.
        /// </summary>
        /// <value>
        /// The mailbox verification.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"MailboxVerification", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"mailboxVerification", Order = 1)]
        public MailboxVerification MailboxVerification { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="EmailAddressSyntaxVerification"/>.
        /// </summary>
        /// <value>
        /// The syntax verification.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"EmailAddressSyntaxVerification", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"syntaxVerification", Order = 2)]
        public EmailAddressSyntaxVerification EmailAddressSyntaxVerification { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="EmailAddressDisposition"/>.
        /// </summary>
        /// <value>
        /// The disposition.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"EmailAddressDisposition", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"disposition", Order = 3)]
        public EmailAddressDisposition EmailAddressDisposition { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Assessments"/>.
        /// </summary>
        [ProtoMember(4)]
        [DataMember(Name = @"Assessments", IsRequired = true, Order = 4)]
        [JsonProperty(PropertyName = @"assessments", Order = 4)]
        public Assessments Assessments { get; set; }
    }
}