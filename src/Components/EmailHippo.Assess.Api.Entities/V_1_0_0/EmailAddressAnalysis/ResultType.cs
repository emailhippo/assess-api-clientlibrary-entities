﻿// <copyright file="ResultType.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.EmailAddressAnalysis
{
    /// <summary>
    /// The Email Address Analysis Result Type.
    /// </summary>
    public enum ResultType
    {
        /// <summary>
        /// <para>No status available.</para>
        /// </summary>
        None = 0,

        /// <summary>
        /// <para>Verification passes all checks including Syntax, DNS, MX, Mailbox, Deep Server Configuration, Grey listing.</para>.
        /// </summary>
        Ok,

        /// <summary>
        /// <para>Verification fails checks for definitive reasons (e.g. mail box does not exist).</para>.
        /// </summary>
        Bad,

        /// <summary>
        /// <para>Conclusive verification result cannot be achieved due to mail server configuration or anti-spam measures.</para>
        /// </summary>
        Unverifiable,

        /// <summary>
        /// <para>The domain is a well known Disposable Email Address (DEA).</para>
        /// </summary>
        /// <remarks>
        /// <para>There are many services available that permit users to use a one-time only email address. Typically, these email addresses are used by individuals wishing to gain access to content or services requiring registration of email addresses but same individuals not wishing to divulge their true identities (e.g. permanent email addresses).</para>
        /// <para>DEA addresses should not be regarded as valid for email send purposes as it is unlikely that messages sent to DEA addresses will ever be read.</para>
        /// </remarks>
        DisposableEmailAddress,
    }
}