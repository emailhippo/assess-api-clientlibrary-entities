﻿// <copyright file="HippoTrust.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.HippoTrust
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// The Hippo Trust.
    /// </summary>
    [ProtoContract]
    [Serializable]
    [DataContract(Name = nameof(HippoTrust))]
    public sealed class HippoTrust
    {
        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        /// <value>
        /// The score.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"Score", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"score", Order = 1)]
        public decimal Score { get; set; }
    }
}