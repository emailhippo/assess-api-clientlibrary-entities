﻿// <copyright file="Registration.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Domain Analysis Registration.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(Registration))]
    [Serializable]
    public sealed class Registration
    {
        /// <summary>
        /// Gets or sets the name of the owner contact.
        /// </summary>
        /// <value>
        /// The name of the owner contact.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"OwnerContactName", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"ownerContactName", Order = 1)]
        public string OwnerContactName { get; set; }

        /// <summary>
        /// Gets or sets the owner contact city.
        /// </summary>
        /// <value>
        /// The owner contact city.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"OwnerContactCity", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"ownerContactCity", Order = 2)]
        public string OwnerContactCity { get; set; }

        /// <summary>
        /// Gets or sets the owner contact country.
        /// </summary>
        /// <value>
        /// The owner contact country.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"OwnerContactCountry", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"ownerContactCountry", Order = 3)]
        public string OwnerContactCountry { get; set; }
    }
}