﻿// <copyright file="DomainDisposition.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using ProtoBuf;

    /// <summary>
    /// Domain Analysis Disposition.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = "DomainDisposition")]
    [Serializable]
    public sealed class DomainDisposition
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is sub domain.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is sub domain; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"IsSubDomain", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"isSubDomain", Order = 1)]
        public bool IsSubDomain { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is gibberish.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is gibberish; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"IsGibberish", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"isGibberish", Order = 2)]
        public bool IsGibberish { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is profanity.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is profanity; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"IsProfanity", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"isProfanity", Order = 3)]
        public bool IsProfanity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is dark web.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is dark web; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(4)]
        [DataMember(Name = @"IsDarkWeb", IsRequired = true, Order = 4)]
        [JsonProperty(PropertyName = @"isDarkWeb", Order = 4)]
        public bool IsDarkWeb { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has been blocked.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has been blocked; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(5)]
        [DataMember(Name = @"HasBeenBlocked", IsRequired = true, Order = 5)]
        [JsonProperty(PropertyName = @"hasBeenBlocked", Order = 5)]
        public bool HasBeenBlocked { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has DNS records.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has DNS records; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(6)]
        [DataMember(Name = @"HasDnsRecords", IsRequired = true, Order = 6)]
        [JsonProperty(PropertyName = @"hasDnsRecords", Order = 6)]
        public bool HasDnsRecords { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has mx records.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has mx records; otherwise, <c>false</c>.
        /// </value>
        [ProtoMember(7)]
        [DataMember(Name = @"HasMxRecords", IsRequired = true, Order = 7)]
        [JsonProperty(PropertyName = @"hasMxRecords", Order = 7)]
        public bool HasMxRecords { get; set; }

        /// <summary>
        /// Gets or sets the mail server location.
        /// </summary>
        /// <value>
        /// The mail server location.
        /// </value>
        [ProtoMember(8)]
        [DataMember(Name = @"MailServerLocation", IsRequired = true, Order = 8)]
        [JsonProperty(PropertyName = @"mailServerLocation", Order = 8)]
        public string MailServerLocation { get; set; }

        /// <summary>
        /// Gets or sets the mail server infrastructure.
        /// </summary>
        /// <value>
        /// The mail server infrastructure.
        /// </value>
        [ProtoMember(9)]
        [DataMember(Name = @"MailServerInfrastructure", IsRequired = true, Order = 9)]
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = @"mailServerInfrastructure", Order = 9)]
        public MailServerInfrastructureType MailServerInfrastructure { get; set; }
    }
}