﻿// <copyright file="Age.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Domain Analysis Age.
    /// </summary>
    [ProtoContract]
    [DataContract(Name = nameof(Age))]
    [Serializable]
    public sealed class Age
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"Text", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"text", Order = 1)]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the seconds.
        /// </summary>
        /// <value>
        /// The seconds.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"Seconds", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"seconds", Order = 2)]
        public long Seconds { get; set; }

        /// <summary>
        /// Gets or sets the iso8601.
        /// </summary>
        /// <value>
        /// The iso8601.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"Iso8601", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"iso8601", Order = 3)]
        public string Iso8601 { get; set; }
    }
}