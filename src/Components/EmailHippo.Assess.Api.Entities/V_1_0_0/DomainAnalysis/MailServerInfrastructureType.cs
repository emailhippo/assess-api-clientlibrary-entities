﻿// <copyright file="MailServerInfrastructureType.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis
{
    /// <summary>
    /// The gender type.
    /// </summary>
    public enum MailServerInfrastructureType
    {
        /// <summary>
        /// The other.
        /// </summary>
        Other,

        /// <summary>
        /// The aol
        /// </summary>
        Aol,

        /// <summary>
        /// The gmail
        /// </summary>
        Gmail,

        /// <summary>
        /// The hostinger.
        /// </summary>
        Hostinger,

        /// <summary>
        /// The hotmail.
        /// </summary>
        Hotmail,

        /// <summary>
        /// The office365.
        /// </summary>
        Office365,

        /// <summary>
        /// The 1
        /// </summary>
        OneAndOne,

        /// <summary>
        /// The alibaba cloud.
        /// </summary>
        AlibabaCloud,

        /// <summary>
        /// The ali mail.
        /// </summary>
        AliMail,

        /// <summary>
        /// The application river.
        /// </summary>
        AppRiver,

        /// <summary>
        /// The barracuda networks.
        /// </summary>
        BarracudaNetworks,

        /// <summary>
        /// The china unicom.
        /// </summary>
        ChinaUnicom,

        /// <summary>
        /// The cisco.
        /// </summary>
        Cisco,

        /// <summary>
        /// The comodo.
        /// </summary>
        Comodo,

        /// <summary>
        /// The exchange defender.
        /// </summary>
        ExchangeDefender,

        /// <summary>
        /// The fire eye.
        /// </summary>
        FireEye,

        /// <summary>
        /// The force point
        /// </summary>
        ForcePoint,

        /// <summary>
        /// The fortinet.
        /// </summary>
        Fortinet,

        /// <summary>
        /// The GMX.
        /// </summary>
        GMX,

        /// <summary>
        /// The google for biz.
        /// </summary>
        GoogleForBiz,

        /// <summary>
        /// The go secure.
        /// </summary>
        GoSecure,

        /// <summary>
        /// The hornet security.
        /// </summary>
        HornetSecurity,

        /// <summary>
        /// The infomaniak.
        /// </summary>
        Infomaniak,

        /// <summary>
        /// The intermedia.
        /// </summary>
        Intermedia,

        /// <summary>
        /// The mail guard.
        /// </summary>
        MailGuard,

        /// <summary>
        /// The mail protector.
        /// </summary>
        MailProtector,

        /// <summary>
        /// The mail route.
        /// </summary>
        MailRoute,

        /// <summary>
        /// The mail ru.
        /// </summary>
        MailRu,

        /// <summary>
        /// The message labs.
        /// </summary>
        MessageLabs,

        /// <summary>
        /// The mimecast.
        /// </summary>
        Mimecast,

        /// <summary>
        /// The network solutions.
        /// </summary>
        NetworkSolutions,

        /// <summary>
        /// The ovh cloud.
        /// </summary>
        OVHCloud,

        /// <summary>
        /// The proofpoint.
        /// </summary>
        Proofpoint,

        /// <summary>
        /// The proton mail.
        /// </summary>
        ProtonMail,

        /// <summary>
        /// The tencent qq.
        /// </summary>
        TencentQQ,

        /// <summary>
        /// The rackspace email hosting.
        /// </summary>
        RackspaceEmailHosting,

        /// <summary>
        /// The retarus.
        /// </summary>
        Retarus,

        /// <summary>
        /// The securence.
        /// </summary>
        Securence,

        /// <summary>
        /// The site ground.
        /// </summary>
        SiteGround,

        /// <summary>
        /// The solar winds.
        /// </summary>
        SolarWinds,

        /// <summary>
        /// The sophos.
        /// </summary>
        Sophos,

        /// <summary>
        /// The spam hero.
        /// </summary>
        SpamHero,

        /// <summary>
        /// The spam titan.
        /// </summary>
        SpamTitan,

        /// <summary>
        /// The swiss COM.
        /// </summary>
        SwissCom,

        /// <summary>
        /// The TOnline.
        /// </summary>
        TOnline,

        /// <summary>
        /// The trend micro.
        /// </summary>
        TrendMicro,

        /// <summary>
        /// The vipre fuse mail.
        /// </summary>
        VipreFuseMail,

        /// <summary>
        /// The yahoo.
        /// </summary>
        Yahoo,

        /// <summary>
        /// The yandex.
        /// </summary>
        Yandex,

        /// <summary>
        /// The zoho.
        /// </summary>
        Zoho,

        /// <summary>
        /// The anti spam g tfor coremail.
        /// </summary>
        AntiSpamGTforCoremail,

        /// <summary>
        /// The blue host.
        /// </summary>
        BlueHost,

        /// <summary>
        /// The host gator.
        /// </summary>
        HostGator,

        /// <summary>
        /// The host monster.
        /// </summary>
        HostMonster,

        /// <summary>
        /// The sonic wall.
        /// </summary>
        SonicWall,

        /// <summary>
        /// The symantec.
        /// </summary>
        Symantec,

        /// <summary>
        /// The trustwave.
        /// </summary>
        Trustwave,

        /// <summary>
        /// The go daddy.
        /// </summary>
        GoDaddy,
    }
}