﻿// <copyright file="DomainAnalysis.cs" company="Email Hippo Ltd">
// © Email Hippo Ltd
// </copyright>

// Copyright 2020 Email Hippo Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace EmailHippo.Assess.Api.Entities.V_1_0_0.DomainAnalysis
{
    using System;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using ProtoBuf;

    /// <summary>
    /// Domain Analysis.
    /// </summary>
    [ProtoContract]
    [Serializable]
    [DataContract(Name = nameof(DomainAnalysis))]
    public sealed class DomainAnalysis
    {
        /// <summary>
        /// Gets or sets the disposition.
        /// </summary>
        /// <value>
        /// The disposition.
        /// </value>
        [ProtoMember(1)]
        [DataMember(Name = @"DomainDisposition", IsRequired = true, Order = 1)]
        [JsonProperty(PropertyName = @"disposition", Order = 1)]
        public DomainDisposition DomainDisposition { get; set; }

        /// <summary>
        /// Gets or sets the age.
        /// </summary>
        /// <value>
        /// The age.
        /// </value>
        [ProtoMember(2)]
        [DataMember(Name = @"Age", IsRequired = true, Order = 2)]
        [JsonProperty(PropertyName = @"age", Order = 2)]
        public Age Age { get; set; }

        /// <summary>
        /// Gets or sets the registration.
        /// </summary>
        /// <value>
        /// The registration.
        /// </value>
        [ProtoMember(3)]
        [DataMember(Name = @"Registration", IsRequired = true, Order = 3)]
        [JsonProperty(PropertyName = @"registration", Order = 3)]
        public Registration Registration { get; set; }
    }
}