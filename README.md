[logo]: https://s3.amazonaws.com/emailhippo/bizbranding/co.logos/eh-horiz-695x161.png "Email Hippo"
[Email Hippo]: https://www.emailhippo.com

![alt text][logo]

# ASSESS API Client

## About
Common entities for [Email Hippo] ASSESS API.